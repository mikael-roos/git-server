How to setup your own Git server
=============================

Some notes on trying to setup my own Git server at home.

[[_TOC_]]



Try 1 - Setup Git server on Ubuntu
------------------------------

Using the article "[4.4 Git on the Server - Setting Up the Server](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server)".

First, you create a git user account and a .ssh directory for that user.

```
sudo adduser git
su git
```

The setup the ssh key area.

```
cd
mkdir .ssh && chmod 700 .ssh
touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
```

Add the users ssh key so they can access the Git server.

```
cat import/user-id_rsa.pub >> ~/.ssh/authorized_keys
```

Repeat for each user that shall use the Git server.

Use `main` as the name for the default branch.

```
git config --global init.defaultBranch main
```

Create an area where you store the Git repositories (using sudo).

```
mkdir /srv/git
chmod 710 /srv/git
chown git.git /srv/git
```

Set up an empty `project` repository for the users by running `git init` with the `--bare` option, which initializes the repository without a working directory.

```
cd /srv/git
mkdir project.git
cd project.git
git init --bare
```

Let a user add a README to the repo and push to it.

```
# On the users computer
mkdir project
cd project
git init
touch README.md
git add .
git commit -a -m "First commit"
git remote add origin git@gitserver:/srv/git/project.git
git push origin main
```

Try cloning it (as any user with access).

```
# On the users computer
git clone git@gitserver:/srv/git/project.git
cd project
git log
```

Ok, so here we have a git user with an area that can be protected by chmod (shared server). But all users (havind their ssh key added) have access to all repos on the server. It is the git user that owns (quota) the files on the server.



Try 2 - Using Git over ssh on a Unix shared server
------------------------------

This server hosts a lot of Unix users with ssh access. Each user can create their own repos and share access to other people using ssh keys.

Create an area on the server where you want to store the git repos.

```
ssh user@sshserver
mkdir git
cd git
```

Use `main` as the name for the default branch.

```
git config --global init.defaultBranch main
```

Create a bare and empty repository.

```
mkdir project.git
cd project.git
git init --bare
```

From your local workstation, init the repo and add a README and push it.

```
# On the users computer
mkdir project
cd project
git init
touch README.md
git add .
git commit -a -m "First commit"
git remote add origin user@sshserver:git/project.git
git push origin main
```

Now you can clone the repo like this.

```
git clone user@sshserver:git/project.git
cd project
```

Now we have a area for git repositorys connected to the local Unix users files and quotas. 

How can we allow that someone else can access that area? Well, we could setup the directory `~user/git` to a specific Unix group, for example `teachers`, so all users of the group can access the area and clone repos from it. That would be a way to ensure that specific unix users have read access to students repos on a shared server.



Try 3 - Adding client and server side hooks
------------------------------

What if I want the repo to be checked prior to pushing, and then when it is pushed onto the server? How can I add my own devops chain to the flow?

Lets figure out how "[8.3 Customizing Git - Git Hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)" works.

__TO BE DONE__



Try 4 - Web interface to the repo
------------------------------

How can we visualize the Git repo, on the shared server where the Unix user host each repo?

Lets check if the cgi script found in "[4.7 Git on the Server - GitWeb](https://git-scm.com/book/en/v2/Git-on-the-Server-GitWeb)" can help out.

__TO BE DONE__



Try 5 - Create a Pull/Merge Request
------------------------------

When the student submits an assignment, it might be helpful to do that using a PR/MR.

__TO BE DONE__



Try 6 - Fork a template repo
------------------------------

When the student initiates a repo (or if it already should be created by the teacher) it might be helpful to for an existing template repo. This could be done using rsync on the server, or perhaps using a fork where the student clones from the template repo, changes the remote to theri own and then starts to work.

__TO BE DONE__



Try 7 - Check out Gitea as an GitHub/GitLab alternative
------------------------------

Gitea is an open source alternative to GitHub/GitLab. [Gitea can be launched using docker](https://docs.gitea.com/installation/install-with-docker) and an `docker-compose.yaml` is included in this repo. Get it and launch Gitea like this.

```
git clone git@gitlab.com:mikael-roos/git-server.git
cd git-server
docker-compose up
```

Now open your browser to `http://localhost:3000/`. 

You will see a installation page, just scroll (optional to add a admin account) and click "Install".

Ok, I got it to work, not to hard. It however needs more investigation to see how it can be used and how the login with users works and how it can connect to different Unix users locally on the server.
